package com.baomidou.springboot.controller;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.springboot.entity.CompanyEmployee;
import com.baomidou.springboot.entity.User;
import com.baomidou.springboot.service.IUserService;

@RestController
@RequestMapping("/user")
public class UserController {

	// 从 application.properties 中读取配置，如取不到默认值为Hello Shanhy
	@Value("${application.hello:Hello Angel}")
	private String hello;

	@Autowired
	private CompanyEmployee ce;

	@Autowired
	private IUserService userService;

	/**
	 * 增删改查 CRUD
	 */
	@RequestMapping("/test1")
	public User test1() {
		System.err.println("删除一条数据：" + userService.deleteById(1L));
		System.err.println("插入一条数据：" + userService.insertSelective(new User(1L, "张三", 17, 1)));
		System.err.println("查询：" + userService.selectById(1L).toString());
		System.err.println("更新一条数据：" + userService.updateSelectiveById(new User(1L, "三毛", 18, 2)));
		return userService.selectById(1L);
	}

	/**
	 * 增删改查 CRUD
	 */
	@RequestMapping("/test11")
	public void test11(HttpServletRequest request, HttpServletResponse response) {
		System.err.println("删除一条数据：" + userService.deleteById(1L));
		System.err.println("插入一条数据：" + userService.insertSelective(new User(1L, "张三", 17, 1)));
		System.err.println("查询：" + userService.selectById(1L).toString());
		System.err.println("更新一条数据：" + userService.updateSelectiveById(new User(1L, "三毛", 18, 2)));
		User user = userService.selectById(1L);
		String json = JSONObject.toJSONString(user);
		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 插入 OR 修改
	 */
	@RequestMapping("/test2")
	public User test2() {
		userService.insertOrUpdateSelective(new User(1L, "王五", 19, 3));
		return userService.selectById(1L);
	}

	/**
	 * 插入 OR 修改
	 */
	@RequestMapping("/test22")
	public void test22(HttpServletRequest request, HttpServletResponse response) {
		userService.insertOrUpdateSelective(new User(1L, "王五", 19, 3));
		// return userService.selectById(1L);
		User user = userService.selectById(1L);
		String json = JSONObject.toJSONString(user);
		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 分页 PAGE
	 */
	@RequestMapping("/test3")
	public Page<User> test3() {
		return userService.selectPage(new Page<User>(0, 12), null);
	}

	/**
	 * 分页 PAGE
	 */
	@RequestMapping("/test33")
	public void test33(HttpServletRequest request, HttpServletResponse response) {
		Page<User> page = userService.selectPage(new Page<User>(0, 12), null);
		String json = JSONObject.toJSONString(page);
		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * @author shaoyongyang
	 * @since 2014-10-13 13:47:46 例子流程
	 **/
	@RequestMapping(value = "/example")
	public ModelAndView example(HttpServletRequest request, Map<String, Object> map) {

		// System.err.println("从 application.properties 中读取配置，如取不到默认值为Hello Shanhy。 ---- " + hello);

		System.err.println(ce.toString());
		System.err.println(ce.getEmployForls().toString());
		System.err.println(ce.getEmployForzs().toString());

		String hello2 = userService.getHello();

		System.err.println("从 userService 实现类中获取。 ---- " + hello2);
		map.put("hello", hello);
		return new ModelAndView("hello");// 当返回hello字符串，会自动去/WEB-INF/jsp/路径下寻找hello.jsp
	}

	@RequestMapping("/changeSessionLanauage")
	public String changeSessionLanauage(HttpServletRequest request, String lang) {
		System.out.println(lang);
		if ("zh".equals(lang)) {
			// 代码中即可通过以下方法进行语言设置
			request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,
					new Locale("zh", "CN"));
		} else if ("en".equals(lang)) {
			// 代码中即可通过以下方法进行语言设置
			request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,
					new Locale("en", "US"));
		}
		return "redirect:/hello";
	}

}
